export default{
  namespaced:true,
  state:()=>({
    // 属性包括
    //  数量 goods_count
    //  商品id goods_id
    //  商品名字 goods_name
    //  商品价格 goods_price 
    //  logo图片 goods_small_logo
    //  选中状态 goods_state 
    // cart:[]
    cart:JSON.parse(uni.getStorageSync('cart') || '[]')
  }),
  // state:{
  //   cart:[]
  // },
      
  
    // state:()=>({
    //   cart:JSON.parse(uni.getStorageSync('cart') || [])
    // }),
    mutations:{
      // 添加购物车
      addCart(state,goods){
        // 找到符合的第一个元素返回他的值,找不到返回undefin
        const findResult = state.cart.find(x=>x.goods_id===goods.goods_id)
       if(!findResult){
         state.cart.push(goods)
       }else{
         findResult.goods_count++
         console.log(findResult)
         this.commit('m_cart/saveStorageCart')
       }
      },
      // 更新商品数量变化
      updateGoodsCount(state,newVal){
        const findResult=state.cart.find(x=>x.goods_id===newVal.goods_id)
       if (findResult) {
               findResult.goods_count = newVal.goods_count
               this.commit('m_cart/saveStorageCart')
             }
      },
      // 商品选中信息变化
      updateGoodsCheck(state,newCheck){
         const findResult=state.cart.find(x=>x.goods_id ===newCheck.goods_id)
         if(findResult){
           findResult.goods_state=newCheck.goods_state
          this.commit('m_cart/saveStorageCart')
         }
      },
      // 商品信息存本地
      saveStorageCart(state){
       uni.setStorageSync('cart', JSON.stringify(state.cart))
       // console.log(JSON.parse(uni.getStorageSync('cart')))
       // const cd=JSON.parse(uni.getStorageSync('cart'))
      },
       // 是否全选
      updateAllCheck(state,newState){
       
         state.cart.forEach(x=>x.goods_state = newState)
           this.commit('m_cart/saveStorageCart')
      }
    },
    getters:{
      // 商品总条数
      total(state){
      return state.cart.reduce((total, item) => total += item.goods_count, 0)
      },
      // 选中商品条数
      checkTotal(state){
      return state.cart.filter(x=>x.goods_state).reduce((total, item) => total += item.goods_count, 0)
      },
      // 商品总价格
      price (state){
        return  state.cart.filter(x=>x.goods_state).reduce((price,item)=>price += item.goods_price*item.goods_count,0)
      },
      // 购物车中已勾选商品的总数量
          checkedCount(state) {
            return state.cart.filter(x => x.goods_state).reduce((total, item) => total += item.goods_count, 0)
          }
    }
}